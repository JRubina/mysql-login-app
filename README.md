# Login App
## Descripción
Éste es un pequeño módulo de uso exclusivamente para pruebas.

## Dependencias
Login App se compone por las siguientes dependencias:
1. [Express](https://expressjs.com/es/)
    * Framework de Node.js para poder construir backend.
2. [Express-handlebars](https://www.npmjs.com/package/express-handlebars)
    * Motor de plantilla para las vistas.
3. [Express-session](https://www.npmjs.com/package/express-session)
    * Dependencia para guardar los datos de session de cada usuario.
4. [Mysql](https://www.npmjs.com/package/mysql)
    * Módulo para conectarnos a la base de datos construida en mysql.
5. [Express-mysql-session](https://www.npmjs.com/package/express-mysql-session)
    * Se utiliza para guardar los datos de sesión en a base de datos.
    * Se recomienda que los datos de sesión se guarden en la base de datos y no en el servidor.
6. [Morgan](https://www.npmjs.com/package/morgan)
    * Dependencia utilizada para generar logs de los usuarios cuando realizan peticiones al servidor.
7. [Bycrptjs](https://www.npmjs.com/package/bcrypt)
    * Dependencia para cifrar las contraseñas de los usuarios antes de ser guardadas en la base de datos.
8. [Passport](http://www.passportjs.org/docs/)
    * Módulo  para autenticar y manejar el proceso de login.
9. [Passport Local](http://www.passportjs.org/packages/passport-local/)
    * Complemento de Passport para realizar una autenticación con nuestra base de datos local.
10. [Timeago.js](https://timeago.org/)
    * Módulo para convertir los timestamps o fechas de la BD en formato de "2hrs ago" ó "2 minute ago".
11. [Connect Flash](https://www.npmjs.com/package/connect-flash)
    * Usado para mostrar mensajes de error y éxito cuando el usuario realice una operación.
12. [Express Validator](https://express-validator.github.io/docs/)
    * Módulo para validar los datos que el usuario os envía desde la aplicación cliente.
    
